function [obstacles, sea_edge] = read_annotations_from_txt(fn, mode)

obstacles = {};
sea_edge = struct();

frame = strsplit(fn,'/'); frame = frame{end};


if mode==0 % read Jon's format
    f_reader = fopen(fn);
    frame = frame(1:end-4);
    % read from my custom format
    
    tmp_line = fgetl(f_reader);
    
    while ischar(tmp_line)
        % do stuff
        if tmp_line(1)=='s'
            s = tmp_line(3:end);
            d = strsplit(s);
            d = [cellfun(@(x) str2num(x), d,'UniformOutput',false)];
            d = [d{:}];
            x=d(1:2:end);
            y=d(2:2:end);
            if isempty(fieldnames(sea_edge))
                sea_edge(end).points = [x; y]';
            else
                sea_edge(end+1).points = [x; y]';
            end
        elseif tmp_line(1)=='r'
            s = tmp_line(3:end);
            d = strsplit(s);
            d = [cellfun(@(x) str2num(x), d,'UniformOutput',false)];
            obstacles(end+1).polygon = [d{:}];
            obstacles(end).frame = frame;
        end
        
        tmp_line = fgetl(f_reader);
        
    end
    
elseif mode==1 % read Borja's format
    f_reader = fopen(fn);
    tmp_line = fgetl(f_reader);
    
    frame = frame(1:end-7);
    while ischar(tmp_line)
        %                     tmp_line
        % Check if current line is for water edge
        if(~isempty(strfind(tmp_line, 'polyline')))
            % use regex to get points points written between [ and ]
            [edgepoint_start_index, edgepoint_end_index] = regexp(tmp_line, '\[(.*?)\]');
            points_x = str2num(tmp_line(edgepoint_start_index(1)+1:edgepoint_end_index(1)-1));
            points_y = str2num(tmp_line(edgepoint_start_index(2)+1:edgepoint_end_index(2)-1));
            sea_edge(end+1).points = [points_x; points_y]';
            % Or obstacle
        elseif(~isempty(strfind(tmp_line, 'rect')))
            % use regex to get info of obstacle
            [obs_start_index, obs_end_index] = regexp(tmp_line, '(:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)');
            % update obstacle list
            obstacles(end+1).polygon = [str2num(tmp_line(obs_start_index(1)+1:obs_end_index(1))), ...
                str2num(tmp_line(obs_start_index(2)+1:obs_end_index(2))), ...
                str2num(tmp_line(obs_start_index(3)+1:obs_end_index(3))), ...
                str2num(tmp_line(obs_start_index(4)+1:obs_end_index(4)))];
            obstacles(end).frame = frame;
        end
        
        tmp_line = fgetl(f_reader);
        
    end
end

fclose(f_reader);




