
# ViAMaRo annotator

## DATA:

- Number of sequences:
	- kope100: 7
	- kope102: 16
	- kope104: 22

- Data is available on [Google Drive](https://drive.google.com/open?id=1EoDYqSRvyqwu5tzT1q771Eeq0ymN9Pc_)
- Results folder is available on [Dropbox](https://www.dropbox.com/sh/40dq3yc7kwya09j/AACC6lC2is-CKdmvmeAwFwoza?dl=0)

## WORKFLOW
- Download and extract image sequences (images for each sequence should be in their own folder).
- Run aggregate_annotations.m on your sequence directory and upload the resulting .mat file to the Dropbox folder so we know who is working on which sequence

- Run Annotator.m
	- You can input the path to the sequence as an argument, otherwise the annotator will open a prompt for selecting a folder

- Annotating obstacles:
	- Select frame you wish to annotate with (left/right or a/s); f to jump to a specific image
	- Select class of new annotation with numbers 1-5
	- n to start the bounding box selection
		- Drag to create bounding box
		- Double-click to fix bounding box
	- e to edit selected bounding box or delete to delete it
	- c to copy the selected bounding box, p to paste it in a different frame
	- y to zoom in on a bounding box for fine corrections (x to unzoom)
	- r to copy all annotations from previous frame (for speed)

- Annotating the sea edge
	- h to initiate annotation
		- Click points on the sea edge from left to right (as many as necessary)
		- Right click to stop adding points
		- Double-click on the line to finish annotation
	- j to edit annotation (you can only move existing points)
	- k to delete annotation

- When you finish with one sequence, run aggregate_annotations.m to save all annotations to one .mat file which you can upload to the Dropbox folder

## RESULTS
- The annotations will be stored in subdirectory named annotations for each sequence. Each frame will have an corresponding .mat file.

## GUIDELINES:
- There are five classes of obstacles: boat, buoy, ship, swimmer and other
	- boat: all sailboats, fishing boats, rowboats etc.
	- buoy: buoys of all sizes, colors and shapes
	- ship: large cruiser and tanker ships only
	- swimmer: self explanatory
	- other: debris, birds and anything else
- Annotate only dynamic obstacles (that excludes boats moored by a pier, but not boats anchored in the bay)
- All obstacles should be annotated as precisely as possible (i.e. the bounding box should contain as much of the obstacle and as little of the background as possible)
- Only the hull of the boat should be annotated (you can ignore masts, except if the entire boat is visible and the sails are open)
- Annotate as finely as possible (create new annotations for partially occluded objects as they come into view)
- Static obstacles should be considered a part of the shore as far as annotating the sea edge is concerned. Dynamic obstacles can be ignored when annotating the sea edge (see the example annotations).


## NOTES
- Keep your annotations consistent.
- Correct your copied annotations: practically none will be at exactly the same position as in the previous frame
- Take care that same physical obstacles have the same label IDs. That only holds for sequential frames, don't keep track of objects once they're out of the viewing angle of the camera. I.e. if an object reappears in camera after several frames it should have a new ID.
- Examples of annotated images are located in directory examples/
	


