classdef Annotator < handle
    properties
        
        %%%%%%%%%%%%%%%%% Annotations folder %%%%%%%%%%%
        annotationsFolderName = 'annotations';
        
        dataset_path
        image_names
        
        figure_main
        
        current_position
        
        I % Current image
        frame
        annotations
        sea_edge
        
        show_annotations = true;
        
        handle_box
        handle_name
        line_handles = cell(1,4);
        sea_edge_handle
        
        
        active_idx
        
        edit_enabled
        
        annotation_file
        copied_annotation
        
        classes = {'boat', 'buoy', 'ship', 'swimmer', 'other'};
        clrs = {[0 0 1], [1 0.64 0], [1 0 0], [0 1 0], [1 1 1]};
        selected_class_idx = 1
        
        current_class_text_handler
        
        fontsize = 20
        text_offset_x = 30
        text_offset_y = 20;
        
    end
    
    methods(Static)
        
        function str = generate_id()
            symbols = ['a':'z' 'A':'Z' '0':'9'];
            stLength = 20;
            nums = randi(numel(symbols),[1 stLength]);
            str = symbols(nums);
        end
    end
    
    
    methods
        function self = Annotator(dataset_path)
            % Dataset path
            if ~exist('dataset_path', 'var') || isempty(dataset_path)
                dataset_path = uigetdir(pwd(), 'Select dataset directory');
            end
            assert(exist(dataset_path, 'dir') ~= 0, 'Invalid dataset path!');
            self.dataset_path = dataset_path;
            
            % Silence an annoying warning
            warning('off', 'Images:initSize:adjustingMag');
            
            % Store dataset path
            self.dataset_path = dataset_path;
            
            % List all images
            contents = dir(fullfile(self.dataset_path, '*'));
            
            self.image_names = {};
            for c = 1:numel(contents)
                if contents(c).isdir
                    continue;
                end
                
                [ ~, ~, ext ] = fileparts(contents(c).name);
                if ismember(ext, { '.jpg', '.png', '.ppm', '.jpeg', '.bmp', '.JPG' })
                    self.image_names{end+1} = contents(c).name;
                end
            end
            
            % create the annotations directory
            if ~exist(strcat(dataset_path,'/',self.annotationsFolderName),'dir')
                mkdir(strcat(dataset_path,'/',self.annotationsFolderName));
            end
            
            % Main figure
            self.figure_main = figure(1);
            
            % setup callbacks
            set(self.figure_main, 'WindowKeyPressFcn', @(w, d) keyboard_callback(self, d));
            set(self.figure_main, 'WindowButtonDownFcn', @(w, d) mouse_callback(self, d));
            
            % Process first frame
            self.current_position = 0;
            process_next_frame(self, 1);
            
        end
        
        function process_next_frame (self, step)
            if step > 0,
                if self.current_position + step > numel(self.image_names),
                    warning('End of images reached!');
                    return;
                end
            elseif step < 0,
                if self.current_position + step < 1,
                    warning('Beginning of images reached!');
                    return;
                end
            end
            
            self.current_position = self.current_position + step;
            
            % Load image
            image_file = fullfile(self.dataset_path, self.image_names{self.current_position});
            self.I = imread(image_file);
            %             t = strsplit(self.image_names{self.current_position}(1:end-4),'_');
            %             self.frame = t{2}; % this should be changed for a different naming scheme
            self.frame = str2num(self.image_names{self.current_position}(1:end-4));
            
            % Try to load annotations
            [ ~, basename ] = fileparts(image_file);
            self.annotation_file = fullfile(self.dataset_path, self.annotationsFolderName, [ basename, '.mat' ]);
            if ~exist(self.annotation_file, 'file'),
                warning('Annotation file "%s" not found!', self.annotation_file);
                %                 self.annotations = struct('class','','id','','polygon',[]);
                self.annotations = [];
                self.sea_edge = [];
                self.save_annotations();
            else
                tmp = load(self.annotation_file);
                self.annotations = tmp.annotations;
                self.sea_edge = tmp.sea_edge;
            end
            
            % Reset selection
            self.active_idx = 0;
            self.edit_enabled = false;
            
            % Update
            update_display(self);
            update_figure_title(self);
        end
        
        function update_display (self)
            % Draw image
            figure(self.figure_main);
            clf();
            imshow(self.I, 'Border', 'tight');
            hold on;
            
            % Update annotations
            self.display_annotations();
            self.update_current_class_selection();
            self.display_sea_edge();
        end
        
        function update_current_class_selection(self)
            % delete old text
            delete(self.current_class_text_handler);
            
            % create new text
            self.current_class_text_handler = text(self.text_offset_x,self.text_offset_y,strcat('new class: ',...
                self.classes{self.selected_class_idx}),'Color','k', 'FontSize', self.fontsize); % maybe not hardcode the offset?
        end
        
        function display_annotation_info(self)
            idx = self.active_idx;
            class = self.annotations(idx).class;
            id = self.annotations(idx).id;
            idx = ismember(self.classes, class);
            line_spacing = 50;
            tab_offset = 40;
            
            x = self.text_offset_x;
            y = self.text_offset_y+line_spacing;
            
            self.line_handles{1} = text(x, y, 'current annotation:', 'Color', 'k', 'FontSize', self.fontsize);
            self.line_handles{2} = text(x+tab_offset, y+line_spacing, class,  'Color', self.clrs{idx}, 'FontSize', self.fontsize);
            self.line_handles{3} = text(x+tab_offset, y+2*line_spacing, strcat('position: ',mat2str(round(self.annotations(self.active_idx).polygon))), ...
                'Color', 'k', 'FontSize', self.fontsize);
            self.line_handles{4} = text(x+tab_offset, y+3*line_spacing, strcat('id: ',id(1:5)),'Color', 'k', 'FontSize', self.fontsize);
        end
        
        function display_annotations (self)
            % Clear old annotations
            delete(self.handle_box(ishandle(self.handle_box)));
            delete(self.handle_name(ishandle(self.handle_name)));
            
            self.handle_box = nan(1, numel(self.annotations));
            
            % set primary window
            figure(self.figure_main);
            
            % Display annotations (except during edit)
            if ~self.edit_enabled && numel(self.annotations)>0
                for i = 1:numel(self.annotations),
                    entry = self.annotations(i);
                    line_width = 2;
                    idx = ismember(self.classes, self.annotations(i).class);
                    color = self.clrs{idx};
                    if i == self.active_idx
                        line_width=line_width+2;
                    end
                    p = entry.polygon;
                    id = self.annotations(i).id;
                    self.handle_box(i) = rectangle('Position',p,'EdgeColor',color,'LineWidth',line_width);
                    self.handle_name(i)=text(p(1), p(2)-20, id(1:5),'Color', color, 'FontSize', self.fontsize);
                end
            end
            
            % Display selected class
            for i = 1:size(self.line_handles,2)
                if(ishandle(self.line_handles{i}))
                    delete(self.line_handles{i});
                end
            end
            
            if self.active_idx > 0
                self.display_annotation_info();
            end
            
            drawnow();
        end
        
        function update_figure_title (self)
            text = sprintf('#%d/%d: %s', self.current_position, numel(self.image_names), self.image_names{self.current_position});
            set(self.figure_main, 'Name', text);
        end
        
        function delete_annotation (self)
            if self.active_idx == 0,
                return;
            end
            
            self.annotations = [self.annotations(1:self.active_idx-1), ...
                self.annotations(self.active_idx+1:end)];
            
            %Update
            self.active_idx = 0;
            self.display_annotations();
            
            %% Save annotations
            self.save_annotations();
            
        end
        
        function new_annotation (self)
            self.edit_enabled = true;
            instanceClassName = self.classes{self.selected_class_idx};
            
            %Get polygon
            polygon_handle = imrect(gca);
            polygon = wait(polygon_handle);
            delete(polygon_handle);
            
            if ~isempty(polygon),
                newAnnoInd = numel(self.annotations) + 1;
                self.annotations(newAnnoInd).frame = self.frame;
                self.annotations(newAnnoInd).class = instanceClassName;
                self.annotations(newAnnoInd).polygon = polygon;
                self.annotations(newAnnoInd).id = self.generate_id();
                
                % Update
                
                %% Save annotations
                self.save_annotations();
            end
            self.edit_enabled = false;
            self.display_annotations();
        end
        
        function edit_annotation (self)
            if self.active_idx == 0,
                return;
            end
            
            self.edit_enabled = true;
            self.display_annotations(); % Update display
            
            figure(self.figure_main);
            
            old_polygon = self.annotations(self.active_idx).polygon;
            
            % Modify polygon
            polygon_handle = imrect(gca, old_polygon);
            polygon = wait(polygon_handle);
            delete(polygon_handle);
            
            if isempty(polygon)
                polygon = old_polygon;
            end
            
            self.edit_enabled = false;
            
            self.annotations(self.active_idx).polygon = polygon;
            self.display_annotations();
            
            %% Save annotations
            self.save_annotations();
        end
        
        function save_annotations(self)
            tmp.annotations = self.annotations;
            tmp.sea_edge = self.sea_edge;
            save(self.annotation_file, '-struct', 'tmp');
        end
        
        function display_sea_edge (self)
            % Delete old handle(s)
            idx = ishandle(self.sea_edge_handle);
            delete(self.sea_edge_handle(idx));
            self.sea_edge_handle = [];
            
            if isempty(self.sea_edge),
                return;
            end
            
            % Display the line
            self.sea_edge_handle = plot(self.sea_edge(:,1), self.sea_edge(:,2), 'y--', 'LineWidth', 2);
        end
        
        function annotate_sea_edge(self)
            % delete old sea edge
            idx = ishandle(self.sea_edge_handle);
            delete(self.sea_edge_handle(idx));
            self.sea_edge_handle = [];
            
            poly = impoly(gca, 'Closed', false);
            xy = wait(poly);
            delete(poly);
            
            if ~isempty(xy),
                % compute the value at image edges if not defined
                if xy(1, 1) > 1,
                    lc = cross([ xy(1,:), 1 ], [ xy(2,:), 1 ]);
                    x = 1;
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ x, y; xy ];
                end
                
                if xy(1, end) < size(self.I, 2),
                    lc = cross([ xy(end-1,:), 1 ], [ xy(end,:), 1 ]);
                    x = size(self.I, 2);
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ xy; x, y ];
                end
                
                self.sea_edge = xy;
            end
            
            % Display sea edge
            self.display_sea_edge();
            self.save_annotations();
        end
        
        function delete_sea_edge(self)
            % delete old sea edge
            idx = ishandle(self.sea_edge_handle);
            delete(self.sea_edge_handle(idx));
            self.sea_edge_handle = [];
            self.sea_edge = [];
            self.save_annotations();
        end
        
        function edit_sea_edge(self)
            poly = impoly(gca, self.sea_edge, 'Closed', false);
            xy = wait(poly);
            delete(poly);
            if ~isempty(xy),
                % compute the value at image edges if not defined
                if xy(1, 1) > 1,
                    lc = cross([ xy(1,:), 1 ], [ xy(2,:), 1 ]);
                    x = 1;
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ x, y; xy ];
                end
                if xy(1, end) < size(self.I, 2),
                    lc = cross([ xy(end-1,:), 1 ], [ xy(end,:), 1 ]);
                    x = size(self.I, 2);
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ xy; x, y ];
                end
                self.sea_edge = xy;
            end
            % Display sea edge
            self.display_sea_edge();
            self.save_annotations();
        end
        
        function copy_annotation (self)
            if self.active_idx == 0,
                return;
            end
            
            self.copied_annotation = self.annotations(self.active_idx);
        end
        
        function paste_annotation(self)
            
            if ~isempty(self.copied_annotation)
                if strcmp(self.copied_annotation.frame,self.frame)==1
                    disp('trying to paste annotation in the same frame');
                else
                    disp('pasted annotation');
                    % edit info and insert into current annotations
                    self.copied_annotation.frame = self.frame;
                    self.annotations = [self.annotations self.copied_annotation];
                    self.display_annotations();
                end
            end
            
            %% Save annotations
            self.save_annotations();
        end
        
        function zoom_surrounding(self)
            %zoom surrounding of an annotation
            margin=50;
            if self.active_idx~=0
                bb=self.annotations(self.active_idx).polygon;
                xl=[bb(1)-10-margin bb(1)+bb(3)+margin];
                yl=[bb(2)-10-margin bb(2)+bb(4)+margin];
                xlim(xl);
                ylim(yl);
            end
        end
        
        function copy_from_previous(self)            
            % get previous image file
            image_file = fullfile(self.dataset_path, self.image_names{self.current_position-1});
            
            % get previous annotations
            [ ~, basename ] = fileparts(image_file);
            annotation_file = fullfile(self.dataset_path, self.annotationsFolderName, [ basename, '.mat' ]);
            
            % copy, modify and save annotations
            if ~exist(annotation_file, 'file')
                warning('Annotation file "%s" not found!', annotation_file);
            else
                tmp = load(annotation_file);
                if ~isempty(self.annotations)
                    for i = 1:numel(tmp.annotations) % add annotations if they don't exist
                        if isempty(find(strcmp({self.annotations.id}, tmp.annotations(i).id)==1))
                            self.annotations = [self.annotations tmp.annotations(i)];
                        end
                    end
                else
                    self.annotations = tmp.annotations;
                end
                
                if isempty(self.sea_edge)
                    self.sea_edge = tmp.sea_edge;
                end
                update_display(self);
            end           
            
        end
        
        function mouse_callback (self, ~)
            if self.edit_enabled,
                return;
            end
            
            pos = get(gca, 'CurrentPoint'); pos = pos(1, 1:2);
            active = arrayfun(@(x) inpolygon(pos(1), pos(2), [x.polygon(1) x.polygon(1)+x.polygon(3) x.polygon(1)+x.polygon(3) x.polygon(1) x.polygon(1)],...
                [x.polygon(2) x.polygon(2) x.polygon(2)+x.polygon(4) x.polygon(2)+x.polygon(4) x.polygon(2)]), self.annotations);
            active = find(active);
            
            if isempty(active)
                self.active_idx = 0;
            else
                self.active_idx = active;
            end
            
            self.display_annotations();
        end
        
        function keyboard_callback (self, event)
            % Do not interfere with built-in keyboard bindings
            if self.edit_enabled,
                return
            end
            
            switch event.Key,
                case { 'a', 'leftarrow' }
                    process_next_frame(self, -1);
                case { 's', 'rightarrow' }
                    process_next_frame(self, +1);
                case { 'f' }
                    answer = inputdlg({ 'Enter image number:' }, 'Jump to image', 1, { num2str(self.current_position) });
                    new_frame = str2double(answer{1});
                    
                    if new_frame < 1 || new_frame > numel(self.image_names)
                        msgbox('Invalid image number', 'Error', 'error');
                        return;
                    end
                    
                    self.current_position = new_frame - 1;
                    process_next_frame(self, +1);
                case { 'e' }
                    self.edit_annotation();
                case { 'delete' }
                    self.delete_annotation();
                case { 'c' }
                    self.copy_annotation();
                case { 'h' }
                    self.annotate_sea_edge();
                case { 'j' }
                    self.edit_sea_edge();
                case { 'k' }
                    self.delete_sea_edge();
                case { 'v' }
                    self.paste_annotation();
                case { 'n' }
                    self.new_annotation();
                case { 'z', 'y' }
                    zoom_surrounding(self);
                case { 'u', 'x' }
                    update_display(self);
                case { '1','2','3','4','5','6','7','8','9' }
                    self.selected_class_idx = min(str2double(event.Key), size(self.classes,2));
                    %self.update_display();
                    self.update_current_class_selection();
                case {'r'}
                    sprintf('r')
                    copy_from_previous(self);
                otherwise
                    event.Key
            end
        end
    end
end
