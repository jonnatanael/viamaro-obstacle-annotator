

close all; clear; clc;

% path = '/home/jon/Downloads/modd3/kope104-00030160-00031540';
path = '/home/jon/Downloads/modd3/kope100-00006790-00007090';
imu_path = 'imu'; imu_files = dir(imu_path); imu_files = imu_files(3:end);
line_width = 2; fontsize = 20;
classes = {'boat', 'buoy', 'ship', 'swimmer', 'other'};
clrs = {[0 0 1], [1 0.64 0], [1 0 0], [0 1 0], [1 1 1]};

% res = aggregate_annotations(path);
% res = load('kope104-00030160-00031540_annotations.mat'); res = res.res;
% res = load('/home/jon/Dropbox/MVL/modd3-obstacle-annotations/kope100-00006790-00007090_annotations.mat');
res = load('/home/jon/Dropbox/MVL/modd3-obstacle-annotations/kope100-00011830-00012500_annotations.mat');
res = res.annotations;

% process_full=true;
process_full=false;

export_images = true;
export_images = false;

if export_images==true
    mkdir('examples');
end

if process_full==true
    images = dir(strcat(path,'/full/*L.jpg'));
    nf=imfinfo(fullfile(path,'/full',images(1).name)); w=nf.Width;
    step = 1;
else
    images = dir(strcat(path,'/frames/*.jpg'));
    nf=imfinfo(fullfile(path,'/frames',images(1).name)); w=nf.Width;
    step = 10;
end

% images = dir(strcat(path,'full/*L.jpg'));
% images = dir(strcat(path,'/frames/*L.jpg'));
% nf=imfinfo(fullfile(path,'/frames',images(1).name)); w=nf.Width;

figure(1); clf;

start = 1;
f = 972.1064; % focal length for stabilization

for i = start:size(images,1)
    i
    
    if process_full==true
        fn = images(i).name;
        im = imread(fullfile(path,'full',fn));
        idx = i;
    else
        fn = images(i+1).name;
        im = imread(fullfile(path,'frames',fn));
        idx = (i*step)+1;
    end
    
%     [ roll, pitch, ~ ] = read_imu_data( fullfile(imu_path,imu_files(i).name));
%     [im_, dy] = stabilize_image(im, roll, pitch, f);
    
    clf;    
    
    rects = [];
    colors = [];
    sea_edge = [];
    
    if (isfield(res{idx}, 'annotations') && ~isempty(res{idx}.annotations))
        ann = res{idx}.annotations;        
        for j = 1:numel(ann)
            p = ann(j).polygon;            
%             p = correct_bbox(p, 0, roll, pitch, f, w);
            clr_ind = ismember(classes, ann(j).class);            
            rects = [rects; p];
            colors = [colors; clrs{clr_ind}];                         
        end   
    else
%         continue
    end    
    
    if (isfield(res{idx}, 'sea_edge') && ~isempty(res{idx}.sea_edge))
        sea_edge = res{idx}.sea_edge; 
        plot(sea_edge(:,1), sea_edge(:,2), 'y--', 'LineWidth', 2);
    end
    
    if export_images==true && ~isempty(sea_edge)
        sea = [];
        for j = 1:size(sea_edge,1)-1
            sea = [sea; sea_edge(j,:) sea_edge(j+1,:)];
        end
        im = insertShape(im,'Line',sea,'LineWidth',3, 'Color','yellow');
        
        for j = 1:size(colors,1)
            im = insertShape(im,'Rectangle',rects(j,:),'LineWidth',5, 'Color',colors(j,:)*255);
        end

        imwrite(im,strcat('exported/',fn));
    else
        imshow(im); hold on;  
        for j = 1:size(colors,1)
            color = colors(j,:);
            p = rects(j,:);
            rectangle('Position',p,'EdgeColor',color,'LineWidth',line_width);
            text(p(1), p(2)-20, classes{clr_ind},'Color', color, 'FontSize', fontsize);
%             text(p(1), p(2)-20, ann(j).id,'Color', color, 'FontSize', fontsize); 
        end
%         plot(sea_edge(:,1), sea_edge(:,2), 'y--', 'LineWidth', 2);
    end    
    
    drawnow;
%     waitforbuttonpress;
    
end