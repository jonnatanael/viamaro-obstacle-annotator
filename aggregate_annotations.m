function annotations = aggregate_annotations( sequence_path )
% loads all annotation files for a sequence and puts them in a single cell array for easier post-processing

% sequence_path = '/home/jon/Downloads/modd3/kope104-00030160-00031540/';
seq_name = strsplit(sequence_path ,'/'); seq_name=seq_name{end};
step = 10;
seq_name

ss = strsplit(seq_name,'-');
start = str2num(ss{2});
finish = str2num(ss{3});

N = finish-start+1;

annotations = cell(1,N);

for i = 1:N
    s.frame = start+(i-1);    
    s.annotations = [];
    s.sea_edge = [];
    s.manual = 0;
    annotations{i}=s;
end


for i = 1:step:N
    
%     fn = images(i).name(1:end-5);
    fn = num2str(annotations{i}.frame,'%08.f');
    annotation_file = strcat(sequence_path,'/frames/annotations/',fn,'.mat');
%     annotation_file = strcat(sequence_path,'/annotations/',fn,'.mat');
    
    if ~exist(annotation_file, 'file'),
        warning('Annotation file "%s" not found!', annotation_file);
        annotations{i}.annotations = [];
        annotations{i}.sea_edge = [];
        annotations{i}.manual = true;
    else
        tmp = load(annotation_file);
%         tmp.annotations
        annotations{i}.annotations = tmp.annotations;
        annotations{i}.sea_edge = tmp.sea_edge;
        annotations{i}.manual = true;
    end
end

save(strcat(seq_name,'_annotations.mat'),'annotations');

end

