
% try to use imu data to rotate images for stabilization
close all; clear; clc;

% testing how to negate roll and pitch displacements
% in order to improve the annotation interpolation

sequence_path = '/home/jon/Downloads/modd3/kope104-00030160-00031540/';
seq_name = strsplit(sequence_path ,'/'); seq_name=seq_name{end-1};

imu_path = 'imu';imu_files = dir(imu_path); imu_files = imu_files(3:end);

images = dir(strcat(sequence_path,'full/*L.jpg'));


% get calibration
files_calib = dir('/home/jon/Desktop/cvww_kalibracija/calib/*.yaml');
filenum = cellfun(@(x)sscanf(x,'calibration-kope%d.yaml'), {files_calib(:).name});
[~,Sidx] = sort(filenum) ;
files_calib = files_calib(Sidx);
idx = 11; % calib104
files_calib(idx).name
calib = cv.FileStorage(strcat('/home/jon/Desktop/cvww_kalibracija/calib/',files_calib(idx).name));

f = calib.M1(1,1);

% v = VideoWriter('stabilized.avi');
% v.FrameRate = 10;
% open(v);

figure(1); clf;

for i = 130:numel(images)
    i
    
    im = imread(fullfile(sequence_path,'full',images(i).name));
    [ roll, pitch, yaw ] = read_imu_data( fullfile(imu_path,imu_files(i).name));
    roll
    
    [im_, dy] = stabilize_image(im, roll, pitch, f); 
    
    res = cat(2,im,im_);
    imshow(res);
    drawnow;
    
    
%     writeVideo(v,res);
    
end

close(v);
