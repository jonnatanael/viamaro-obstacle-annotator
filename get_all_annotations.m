

% gets a .mat file of aggregated annotations for each sequence and zips them together

close all; clear; clc;

path = '/home/jon/Downloads/modd3/';
path = '/media/jon/spaaace/modd3_annotation/'

sequences = dir(path); sequences = sequences(3:end);
annotations={};
seq_list = {};

for i = 1:numel(sequences)
    
    s = sequences(i).name;
    if ~isdir(fullfile(path,s))
        continue
    end
    
    fullfile(path,s)
    ann = aggregate_annotations(fullfile(path,s));
%     if ~isempty(ann)
%         fullfile(path,s)
%         ann
%     end
    
    seq_list{end+1}=s;
    annotations{end+1}=ann;
    
end

annotations
a=annotations{1};
a{1:end}