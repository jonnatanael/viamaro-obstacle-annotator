

close all; clear; clc;

% figure out how annotation bboxes move when a lot of roll is present


i = 209;

sequence_path = '/home/jon/Downloads/modd3/kope104-00030160-00031540/';
seq_name = strsplit(sequence_path ,'/'); seq_name=seq_name{end-1};

imu_path = 'imu';imu_files = dir(imu_path); imu_files = imu_files(3:end);

images = dir(strcat(sequence_path,'full/*L.jpg'));


% get calibration
files_calib = dir('/home/jon/Desktop/cvww_kalibracija/calib/*.yaml');
filenum = cellfun(@(x)sscanf(x,'calibration-kope%d.yaml'), {files_calib(:).name});
[~,Sidx] = sort(filenum) ;
files_calib = files_calib(Sidx);
idx = 11; % calib104
files_calib(idx).name
calib = cv.FileStorage(strcat('/home/jon/Desktop/cvww_kalibracija/calib/',files_calib(idx).name));

f = calib.M1(1,1);

p=[1220 493 58 25];
p=[843 509 77 28];
p=[76 528 83.9999999999999 27];
p=[270.727710843373 509.525301204819 337.207228915663 98.544578313253];

im = imread(fullfile(sequence_path,'full',images(i).name));
[h,w,~]=size(im);
[ roll, pitch, yaw ] = read_imu_data( fullfile(imu_path,imu_files(i).name));
[im_, dy] = stabilize_image(im, roll, pitch, f);

p2 = correct_bbox(p, 0, roll, pitch, f, w);

figure(1); clf;
subplot(121);
imshow(im);
hold on;
rectangle('Position',p,'EdgeColor','r','LineWidth',3);


subplot(122);
imshow(im_);
hold on;
rectangle('Position',p2,'EdgeColor','r','LineWidth',3);


   
    