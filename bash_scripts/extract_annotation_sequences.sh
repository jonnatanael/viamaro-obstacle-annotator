#!/bin/bash

echo $0
echo $1

# make a list of frame ranges

# path="/home/jon/Downloads/coln_raw_data/"
#output_file="/home/jon/Downloads/modd3/"
path="/media/jon/Scratch/"
output_file="/media/jon/Home/viamaro_data/modd3_test/"
step=10
program="/home/jon/usv/bin/mvl-stereo-processor"

# kope100
# path="/media/jon/Scratch/"
# sequence="kope100"
# data_file="video00.vrms"
# f1=(6790 11830 16700 20780 21220 31860 35670)
# f2=(7090 12500 17200 21180 22120 32500 35900)


# kope101
#path="/media/jon/Scratch/"
#sequence="kope101"
#data_file="video01.vrms"
#f1=(4130 7150 10680 15100 15960 16760 17880 20790 31760 33000 38550 41600 43600 51300 55300 60000  )
#f2=(4650 7620 11250 15750 16160 17240 18500 22680 32200 33300 39100 42000 44200 51670 55900 60400  )

# kope102



# kope103
# path="/media/jon/Home/viamaro_data/"
# sequence="kope103"
# data_file="video02.vrms"
# f1=(1050 3450 5550 7430 9280 11000 12140 13900 14700 16600 18120 20600 23400 25200 25800 26610 27230 28030 30190 32520 36460 39900 42560 45730 46750 49200 50500 51300 52430 54020 54810 56580 57010 58050 59850 61230 63750 64110 65520 71510 73890 76500  )
# f2=(1400 4800 6850 8100 10000 12000 13500 14500 16400 17600 19680 22400 24890 25800 26410 26940 27400 30100 32000 34550 39500 41200 44420 46500 48930 50140 50990 52210 52510 54500 55810 56950 57200 58850 61070 62120 63910 64450 65900 72010 75000 77030  )


# path="/media/jon/Home/viamaro_data/"
# sequence="kope103"
# data_file="video03.vrms"
# f1=(800 3000 6100 7600 )
# f2=(1400 3250 7300 8500 )


# kope104



# stru01

# path="/media/jon/Home/viamaro_data/"
# output_file="/media/jon/Home/viamaro_data/stru01_test/"
# sequence="stru01"
# data_file="video00.vrms"
# f1=(1 2090)
# f2=(1400 3220)

# path="/media/jon/Home/viamaro_data/"
# sequence="stru01"
# data_file="video01.vrms"
# f1=(6500 7410 8610 10160 )
# f2=(6650 8100 10030 11075 )

# stru02
# path="/media/jon/Home/viamaro_data/"
# sequence="stru02"
# data_file="video00.vrms"
# f1=(2305 3200 4700 6700 7760 10810 11440 12400 13150 15700 16655 17200 19700 22300 24200 24630 27600 30250 32800 38000 40300 41000 41450 41800 43360 44220 47000 47860 53360 56180 57800 59300 62430 64680 69690 71300 72370 73800 74550 76230 77320 78630 79900 80600 81400 83140 84740 86460 86970 88350 97380 99710 108050 108720 110060 111210 111850 113120 114220 116500 118250 119000 120350 144650 )
# f2=(2750 4150 5400 7500 7924 11180 12220 12600 14600 16300 17075 18500 20000 23110 24460 25400 28800 31660 33600 39300 40700 41320 41700 42750 44180 44900 47720 48000 53940 56880 58500 59900 63100 65180 70310 72000 73190 74100 74900 76800 77580 79230 80410 81010 82400 83450 85290 86800 87370 88690 97740 99970 108320 109150 110500 111600 112250 113420 114400 117000 118800 119450 120650 144950  )

#path="/media/jon/Home/viamaro_data/"
#output_file="/media/jon/Home/viamaro_data/stru02_extracted/"
#sequence="stru02"
#data_file="video00.vrms"
#f1=(10000)
#f2=(20000)

sequence="kope100"
f1=(6790 11830)
# f2=(12500)
f2=(6790 11830)
data_file="video00.vrms"
echo $path$sequence/$data_file

calibration_file="/home/jon/usv/calibration-"$sequence".yaml"
# calibration_file="/home/jon/usv/calibration-kope100.yaml"

# echo $path$sequence/$data_file

# remove previous runs
#rm -rf $output_file

mkdir $output_file

for i in "${!f1[@]}"; do
	printf -v start "%08d" ${f1[i]}
	printf -v stop "%08d" ${f2[i]}
	echo $sequence-$start-$stop
	mkdir $output_file$sequence-$start-$stop/
	mkdir $output_file$sequence-$start-$stop/frames
	$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-rectified=$output_file$sequence-$start-$stop"/rectified/%{f|08d}%{s}.jpg" --output-frames=$output_file$sequence-$start-$stop"/frames/%{f|08d}%{s}.jpg" --stereo-calibration=$calibration_file
	#$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-rectified=$output_file$sequence-$start-$stop"/full/%{f|08d}%{s}.jpg" --stereo-calibration=$calibration_file
	# remove right images
	find $output_file$sequence-$start-$stop"/frames/" -name "*R.jpg" -delete
	# rename left images
	rename 's/L.jpg/.jpg/' $output_file$sequence-$start-$stop/frames/*.jpg*
done

