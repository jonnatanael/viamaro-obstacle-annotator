#!/bin/bash

echo $0
echo $1

# make a list of frame ranges

step=50
output_file="/home/jon/Downloads/modd3_iros_extra/"
program="/home/jon/usv/bin/mvl-stereo-processor"


path="/media/jon/Home/viamaro_data/"
sequence="kope90"
data_file="video00.vrms"
f1=(30000)
f2=(40000)


# path="/media/jon/Scratch/"
# sequence="kope100"
# data_file="video00.vrms"
# f1=(6790 11830 16700 20780 21220 31860 35670)
# f2=(7090 12500 17200 21180 22120 32500 35900)


calibration_file="/home/jon/usv/calibration-"$sequence".yaml"

#echo $path$sequence/$data_file

# remove previous runs
rm -rf $output_file

mkdir $output_file

for i in "${!f1[@]}"; do
	printf -v start "%08d" ${f1[i]}
	printf -v stop "%08d" ${f2[i]}
	echo $sequence-$start-$stop
	rm -rf $output_file$sequence-$start-$stop/full
	mkdir $output_file$sequence-$start-$stop/
	mkdir $output_file$sequence-$start-$stop/full

	# rectified
	#$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-rectified=$output_file$sequence-$start-$stop"/full/%{f|08d}%{s}.jpg" --stereo-calibration=$calibration_file

	# not-rectified
	$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-frames=$output_file$sequence-$start-$stop"/full/%{f|08d}%{s}.jpg"

	# remove right images
	#find $output_file$sequence-$start-$stop"/frames/" -name "*R.jpg" -delete
	# rename left images
	#rename 's/L.jpg/.jpg/' $output_file$sequence-$start-$stop/frames/*.jpg*
done

