#!/bin/bash

path="/media/jon/Scratch/"
# path="/media/jon/Home/viamaro_data/"
step=10
program="/home/jon/usv/bin/mvl-stereo-processor"
output_file="/media/jon/Home/viamaro_data/modd3_test/"

declare -a sequences=("kope100" "kope101" "kope102" "kope103" "kope104" "stru01" "stru02")

# for i in ${sequences[*]}; do
# 	echo $i
# done

# kope100
# sequence="kope100"
# f1=(6790 11830 16700 20780 21220 31860 35670)
# f2=(7090 12500 17200 21180 22120 32500 35900)
# calibration_file="/home/jon/usv/calibration-"$sequence".yaml"
# data_file="video00.vrms"
# echo $path$sequence/$data_file

sequence="kope100"
f1=(11830)
f2=(12500)
calibration_file="/home/jon/usv/calibration-"$sequence".yaml"
data_file="video00.vrms"
echo $path$sequence/$data_file

mkdir $output_file

for i in "${!f1[@]}"; do
	printf -v start "%08d" ${f1[i]}
	printf -v stop "%08d" ${f2[i]}
	echo $sequence-$start-$stop
	# mkdir $output_file$sequence-$start-$stop/
	# mkdir $output_file$sequence-$start-$stop/frames
	$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-rectified=$output_file$sequence-$start-$stop"/frames/%{f|08d}%{s}.jpg" --stereo-calibration=$calibration_file
	#$program $path$sequence/$data_file --frame-range ${f1[i]}:$step:${f2[i]} --output-rectified=$output_file$sequence-$start-$stop"/full/%{f|08d}%{s}.jpg" --stereo-calibration=$calibration_file
	# remove right images
	# find $output_file$sequence-$start-$stop"/frames/" -name "*R.jpg" -delete
	# rename left images
	# rename 's/L.jpg/.jpg/' $output_file$sequence-$start-$stop/frames/*.jpg*
done

# list the .vrms files used

# list start/end frames

# loop and extract