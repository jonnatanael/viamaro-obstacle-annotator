#!/bin/bash

# zips every sequence then zips everything together for easier up/downloading

path="/home/jon/Downloads/modd3"

mkdir $path/../modd3_annotation

for dir in $path/*; do   # list directories in the form "/tmp/dirname/"
    seq=${dir##*/}
    echo $seq
    zip -r -j $path/../modd3_annotation/$seq.zip $dir/frames/
done

zip -j $path/../modd3_annotation.zip $path/../modd3_annotation/*

