
% finds manual annotations and performs interpolation using CLRS (or w/e) tracker;

close all; clear; clc;

path = '/home/jon/Downloads/modd3/kope104-00030160-00031540/';
imu_path = 'imu';imu_files = dir(imu_path); imu_files = imu_files(3:end);
seq_name = strsplit(path ,'/'); seq_name=seq_name{end-1};
line_width = 2; fontsize = 20;
step = 10;
classes = {'boat', 'buoy', 'other','ship','yacht','swimmer','bird'};
clrs = {[0 0 1], [1 0.64 0], [1 0 0], [0 1 0], [1 1 1], [1 1 0], [1 0 1]};

res = aggregate_annotations(path);

% get calibration
files_calib = dir('/home/jon/Desktop/cvww_kalibracija/calib/*.yaml');
filenum = cellfun(@(x)sscanf(x,'calibration-kope%d.yaml'), {files_calib(:).name});
[~,Sidx] = sort(filenum);files_calib = files_calib(Sidx);idx = 11; % calib104
calib = cv.FileStorage(strcat('/home/jon/Desktop/cvww_kalibracija/calib/',files_calib(idx).name));

f = calib.M1(1,1);

images = dir(strcat(path,'full/*L.jpg'));
N = numel(images);
% figure(1); clf;

for i = 1:step:N
    i
    
    if i == 131
        i
    elseif i < 131 || i > 131
        continue;
    end
%     res{i}
    % check if current and next frame have annotations
    if (isfield(res{i}, 'annotations') && ~isempty(res{i}.annotations) && isfield(res{i+step}, 'annotations') && ~isempty(res{i+step}.annotations))
        ann1 = res{i}.annotations;
        ann2 = res{i+step}.annotations;
        
%         for j = 1:numel(ann1)
        for j = 1
            id = ann1(j).id;
            index = find(strcmp({ann2.id},id)==1);
            if ~isempty(index)
                p1 = ann1(j).polygon;
                p2 = ann2(index).polygon;
                [ roll, pitch, ~ ] = read_imu_data( fullfile(imu_path,imu_files(i-1).name));
                nf=imfinfo(fullfile(path,'full',images(i).name)); w=nf.Width;
                im = imread(fullfile(path,'full',images(i).name));
                
%                 imshow(im);
%                 rectangle('Position',p1,'EdgeColor','r','LineWidth',3);
%                 drawnow;
%                 waitforbuttonpress;
                
%                 dy = f*tand(pitch);
                P1 = correct_bbox(p1, 0, roll, pitch, f, w);
                P2 = correct_bbox(p2, 0, roll, pitch, f, w);
                
                % get image list
                lst = {};
                lst_stable = {};
                dys = {};
                r = {};
                pt = {};
                for k = 1:step
                    [ roll, pitch, ~ ] = read_imu_data( fullfile(imu_path,imu_files(i+(k-1)).name));
                    im = imread(fullfile(path,'full',images(i+(k-1)).name));
                    
                    [im_, dy] = stabilize_image(im, roll, pitch, f);
                    lst{end+1}=im;
                    r{end+1}=roll;
                    pt{end+1}=pitch;
                    lst_stable{end+1} = im_;
%                     imshow(lst{k});
%                     drawnow;
                end
                

%                 subplot(121);
%                 imshow(lst{1}); hold on;
%                 rectangle('Position',correct_bbox(p1, 1, r{1}, pt{1}, f, w),'EdgeColor','r','LineWidth',3);
%                 subplot(122);
%                 imshow(lst_stable{1}); hold on;
%                 rectangle('Position',p1,'EdgeColor','r','LineWidth',3);
%                 drawnow;
%                 waitforbuttonpress;
                
                
%                 size(lst)
                % perform tracking
                sprintf('tracking forward..')
                poly1 = track(lst_stable,P1);
                
                sprintf('tracking backward..')
                poly2 = track(lst_stable(end:-1:1),P2);
                poly2 = flipud(poly2);
                
                
%                 for k = 1:step-1
%                     % remove stabilization offset
% %                     pt{k}
%                     p1 = poly1(k,:);
% %                     p1 = correct_bbox(p1, 1, r{k}, pt{k}, f, w);    
%                     p1 = correct_bbox(p1, 1, r{k}, pt{k}, f, w);
%                     p2 = poly2(k,:);
%                     p2 = correct_bbox(p2, 1, r{k}, pt{k}, f, w);
%                     
%                     p = 0.5*p1+0.5*p2;
% %                     p = p1;
%                     imshow(lst{k}); hold on;
% %                     imshow(lst_stable{k}); hold on;
% %                     rectangle('Position',P1,'EdgeColor','g','LineWidth',3);
%                     rectangle('Position',p,'EdgeColor','r','LineWidth',3);
%                     drawnow;
% %                     waitforbuttonpress;
%                 end
                
                for k = 1:step-1                   
                    
                    p1 = poly1(k,:); p1 = correct_bbox(p1, 1, r{k}, pt{k}, f, w);
                    p2 = poly2(k,:); p2 = correct_bbox(p2, 1, r{k}, pt{k}, f, w);
                    
                    a.polygon = 0.5*p1+0.5*p2;
                    a.class = ann1(j).class;
                    a.frame = res{i+k}.frame;
                    a.id = ann1(j).id;
                    if ~isfield(res{i+k}, 'annotations')
                        res{i+k}.annotations = struct('');
                    end                    
                    res{i+k}.annotations = [res{i+k}.annotations a];
                    res{i+k}.manual = false;
                end
                
            end
        end
    end
end

save(strcat(seq_name,'_annotations.mat'),'res');
