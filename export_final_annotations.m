

close all; clear; clc;

% load annotations for single image
pth = '/media/jon/Home/viamaro_data/modd3/';
% pth = '/media/jon/Home/viamaro_data/modd3_full/';
output_dir = 'annotations_final'

sequences = dir(strcat(pth,'*-*'));

for i = 1:numel(sequences)
% for i = numel(sequences)-1
% for i = 178-21
% for i = 95
    
    seq = sequences(i);
    
    if ~strcmp(seq.name,'calibration')
        seq
        
        ann = dir(strcat(pth,seq.name,'/annotations/*.mat'));
%         ann=ann(3:end);
        
        mkdir(fullfile(pth,seq.name,output_dir));
        
        for j = 1:numel(ann)
            sea_edge = {};
            obstacles = {};
            
            % load annotation for a frame
            load(fullfile(pth, seq.name,'annotations',ann(j).name))
            
            fn = ann(j).name; fn = fn(1:end-4);
            if fn(end)=='L'
                fn = fn(1:end-1);
            end
            fn
            
%             continue;
            
            % open output file
%             strcat(pth, seq.name,'/annotations/',fn,'.txt')
            fid = fopen(strcat(pth, seq.name,'/annotations_final/',fn,'.txt'),'w');
            
            % write sea edge
            if isstruct(sea_edge)
                if isfield(sea_edge,'points')
                    for k = 1:numel(sea_edge)
                        se = sea_edge(k).points';
                        if ~isempty(se)
                            se = round(se);
                            ls = se(:);
                            fprintf(fid,'s ');
                            fprintf(fid,'%d ',ls);
                            fprintf(fid,'\n');  
                        end                        
                    end
                end
            else
                % convert sea edge to struct and save
                tmp = sea_edge;
                sea_edge={};
                sea_edge.points = tmp;
                for k = 1:numel(sea_edge)
                    if ~isempty(sea_edge(k).points)
                        se = sea_edge(k).points';
                        se = round(se);
                        ls = se(:);
                        fprintf(fid,'s ');
                        fprintf(fid,'%d ',ls);
                        fprintf(fid,'\n');  
                    end                    
                end
%                 break
            end       
            
            % write obstacles
            for k = 1:numel(obstacles)
                if isfield(obstacles,'polygon') && ~isempty(obstacles(k).polygon)
                    o = round(obstacles(k).polygon);
                    fprintf(fid,'r %d %d %d %d\n', o);
    %                 ls = se(:);
    %                 fprintf(fid,'s ');
    %                 fprintf(fid,'%d ',ls);
                end
                
            end           
            
            fclose(fid);
            
        end
        
%         break
    end
    
end

