
close all; clear; clc;

set(0,'DefaultFigureWindowStyle','docked');
warning('off','all')

% load annotations for single image
pth = '../modd3_full';

sequences = dir(pth); sequences=sequences(3:end); sequences=sequences(3:end);

% dataset_path = '/media/jon/Home/viamaro_data/modd3/kope102-00022135-00022435';
%
% dataset_path_txt = fullfile(dataset_path, 'annotations_txt');
%
% ann = dir(fullfile(dataset_path,'annotations_txt/*.txt'));
%
% a = '00022135_00.txt';
% fn = strsplit(a,'_'); fn = fn{1}
%
% im_pth = strcat(dataset_path,'/frames/',fn,'.jpg');
% im = imread(im_pth);
%
% figure(1); clf;
% read_txt_and_plot(strcat(dataset_path_txt,'/',a), im, fn)

% return

figure(1);
cnt = 0;
all = 0;

full_cnt = 0;

res = [];

for i = 1:size(sequences,1)
% for i = 15:size(sequences,1)
% for i = 2
    i
    
    sequence_id = sequences(i).name
    dataset_path = fullfile(pth, sequence_id);
    dataset_path_txt = fullfile(dataset_path, 'annotations_txt');
    
    ann = dir(fullfile(dataset_path,'annotations_txt/*_*.txt'));
    %     ann(:).name
    
    sequence_id_split = strsplit(sequence_id, '-');
    sequence_parent = sequence_id_split{1};
    sequence_frame_start = str2double(sequence_id_split{2});
    sequence_frame_end = str2double(sequence_id_split{3});
    
    cnt = 0;
    
    for sequence_frame_current = sequence_frame_start : 10 : sequence_frame_end
%         sequence_frame_current
        
        
        
        all=all+1;
        
        %tmp_img_name = strsplit(listing_dir(i).name, '.');
        %tmp_img_name = tmp_img_name{1};
        listing_corresponding_txts = dir(fullfile(dataset_path_txt, sprintf('%08d_*.txt', sequence_frame_current)));
        
        res = [res; numel(listing_corresponding_txts)];
        continue
        
        tmp_img_name = sprintf('%08dL.jpg', sequence_frame_current);
        
        if(numel(listing_corresponding_txts) > 1)
%             listing_corresponding_txts
%             continue
            clf;
            tmp_img = imread(fullfile(dataset_path, 'frames',tmp_img_name));
%             tmp_img = imread(fullfile(dataset_path, 'rectified',tmp_img_name));
            fprintf('Showing annotation results for image number: %08d\n', sequence_frame_current);
            
            numel(listing_corresponding_txts)
            for j = 1:numel(listing_corresponding_txts)
%                 j
                tmp_txt_name = listing_corresponding_txts(j).name;
                txt_path = fullfile(dataset_path_txt, tmp_txt_name);
                read_txt_and_plot(txt_path, tmp_img, listing_corresponding_txts(end).name);
                drawnow;
                waitforbuttonpress
            end
            
%             tmp_txt_name = listing_corresponding_txts(end).name; % only display final
%             txt_path = fullfile(dataset_path_txt, tmp_txt_name);

%             read_txt_and_plot(txt_path, tmp_img, listing_corresponding_txts(end).name);
%             f_reader = fopen(txt_path);

        else
            cnt=cnt+1;
        end
    end
    
%     int32([cnt size(sequence_frame_start : 10 : sequence_frame_end,2) cnt/size(sequence_frame_start : 10 : sequence_frame_end,2)])
    
%     if cnt/size(sequence_frame_start : 10 : sequence_frame_end,2)>0.9
%     if int32(cnt/size(sequence_frame_start : 10 : sequence_frame_end,2))==1
%         full_cnt=full_cnt+1;
%     end
%     
%     a = '00006790_05.txt';
%     fn = strsplit(a,'_'); fn = fn{1}
%     
%     im_pth = strcat(dataset_path,'/frames/',fn,'.jpg');
%     im = imread(im_pth);
%     
%         figure(1); clf;
%         imshow(im);
    
%     break
    
end

res

% [cnt all, cnt/all]

% read image

% display annotations

