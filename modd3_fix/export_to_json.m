

close all; clear; clc;

pth = '/media/jon/disk1/viamaro_data/modd3_full/';
d=dir(pth); d(1:3)=[];
% d

i = 10;


seq = sprintf("%s%s", pth, d(i).name);


a = struct();

a.file_name = 'abcd.jpg';
a.all_annotations = 'true';

a.obstacles = {};
o = struct();
o.id = 0;
o.type = 'boat';
o.bbox = [0 0 1 1];
o.area = 1;
a.obstacles{end+1}=o;
a.obstacles{end+1}=o;

a.water_edges = {};
e = struct();
e.id = 0;
e.x_axis = [0 1 2];
e.y_axis = [0 1 2];

a.water_edges{end+1}=e;

a.danger_zone = {};
d = struct();
d.id = 0;
d.x_axis = [0 1 2];
d.y_axis = [0 1 2];

a.danger_zone{end+1}=d;

b=jsonencode(a)
% b = jsonencode(b);
% b = strrep(b, ',', sprintf(',\r'));
% b = strrep(b, '[{', sprintf('[\r{\r'));
% b = strrep(b, '}]', sprintf('\r}\r]'));
% b
