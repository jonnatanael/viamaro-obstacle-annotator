

close all; clear; clc;

url = 'https://vision.fe.uni-lj.si/~duskov/robocoln/data/raid/modd3/'
pth = 'C:\Users\Jon\Desktop\LSI\modd3_full\';
options = weboptions('Username','robocoln','Password','podatki');

d=dir(pth); d(1:3)=[];
d

for i = 1
    
    seq = sprintf("%s%s", pth, d(i).name);
    image_list = dir(sprintf('%s/frames/*L.jpg', seq));
    
    for j = 1:numel(image_list)
        
        img_fn = image_list(j).name;
        img_fn = strcat(img_fn(1:end-5),'.jpg')
        s = sprintf('%s%s/frames/%s', url, d(i).name, img_fn);
        img = webread(s, options);
        imshow(img);
        waitforbuttonpress
        
    end
    
    
    
end

return

% pth = 'https://vision.fe.uni-lj.si/~duskov/robocoln/data/raid/kope/kope104/slike/IMG_6042.JPG'
url = 'https://vision.fe.uni-lj.si/~duskov/robocoln/data/raid/modd3/'

% [gzFileName,st] = urlwrite(pth,fn, 'Authentication', 'Basic', ...
%   'Username', 'robocoln', 'password', 'podatki');

% [a,info] = urlread_auth(pth, 'robocoln', 'podatki');

options = weboptions('Username','robocoln','Password','podatki');
% img = webread(pth, options);
% data = webread(url, options);

% size(img)

% return
% imshow(img);