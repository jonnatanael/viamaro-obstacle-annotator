classdef AnnotatorModd3 < handle
    properties
        
        %%%%%%%%%%%%%%%%% Annotations folder %%%%%%%%%%%
        annotationsFolderName = 'annotations';
        
        dataset_path
        image_names
        
        figure_main
        
        current_position
        
        I % Current image
        frame
        obstacles
        sea_edge
        
        show_annotations = true;
        
        handle_box
        handle_name
        line_handles = cell(1,4);
        sea_edge_handle
        
        active_type % if 0, obstacle, else sea
        active_idx
        
        edit_enabled
        
        annotation_file
        copied_annotation
        
        classes = {'ship', 'person', 'other'};
        clrs = {[0 0 1], [1 0.64 0], [1 0 0], [0 1 0], [1 1 1]};
        selected_class_idx = 1
        
        current_class_text_handler
        
        fontsize = 20
        text_offset_x = 30
        text_offset_y = 20;
        global_class = '';
        
    end
    
    
    methods
        function self = AnnotatorModd3(dataset_path)
            % Dataset path
            if ~exist('dataset_path', 'var') || isempty(dataset_path)
                dataset_path = uigetdir(pwd(), 'Select dataset directory');
            end
            assert(exist(dataset_path, 'dir') ~= 0, 'Invalid dataset path!');
            self.dataset_path = dataset_path;
            
            % Silence an annoying warning
            warning('off', 'Images:initSize:adjustingMag');
            
            % Store dataset path
            self.dataset_path = dataset_path;
            
            % List all images
            contents = dir(fullfile(self.dataset_path, '/frames/*L.jpg'))
            
            self.image_names = {};
            for c = 1:numel(contents)
                if contents(c).isdir
                    continue;
                end
                
                [ ~, ~, ext ] = fileparts(contents(c).name);
                if ismember(ext, { '.jpg', '.png', '.ppm', '.jpeg', '.bmp', '.JPG' })
                    self.image_names{end+1} = contents(c).name;
                end
            end
            
            % create the annotations directory
            if ~exist(strcat(dataset_path,'/',self.annotationsFolderName),'dir')
                mkdir(strcat(dataset_path,'/',self.annotationsFolderName));
            end
            
            % Main figure
            self.figure_main = figure(1);
            
            % setup callbacks
            set(self.figure_main, 'WindowKeyPressFcn', @(w, d) keyboard_callback(self, d));
            set(self.figure_main, 'WindowButtonDownFcn', @(w, d) mouse_callback(self, d));
            
            % Process first frame
            self.current_position = 0;
            process_next_frame(self, 1);
            
        end
        
        function process_next_frame(self, step)
            if step > 0,
                if self.current_position + step > numel(self.image_names),
                    warning('End of images reached!');
                    return;
                end
            elseif step < 0,
                if self.current_position + step < 1,
                    warning('Beginning of images reached!');
                    return;
                end
            end
            
            self.current_position = self.current_position + step;
            
            % Load image
            image_file = fullfile(self.dataset_path, 'frames',self.image_names{self.current_position})
            self.I = imread(image_file);
           
            %             t = strsplit(self.image_names{self.current_position}(1:end-4),'_');
            %             self.frame = t{2}; % this should be changed for a different naming scheme
            self.frame = str2num(self.image_names{self.current_position}(1:end-5));
            
            self.load_annotations();            
            
            % Reset selection
%             self.active_idx = 0;
            self.edit_enabled = false;
            
            self.active_type = 0;
            if ~isempty(self.obstacles)
                self.active_idx = 1;
            else
                self.active_idx = 0;
                % uncomment to skip images with no obstacles
%                 if self.current_position + step <= numel(self.image_names)
%                     process_next_frame(self, 1);
%                     return                    
%                 end                
                
            end
            
            % Update
            update_display(self);
            update_figure_title(self);
        end
        
        function set_global_class(self)
            pos = self.current_position;
            
            for i = 1:size(self.image_names, 2)
                self.current_position = i;
                self.load_annotations();
                for j = 1:numel(self.obstacles)
                    self.obstacles(j).class = self.global_class;
                end
                self.save_annotations();
            end
            
            self.current_position = pos;
            process_next_frame(self,0);
            
        end
        
        function extrapolate_sea(self, direction)
            
            if direction==1
                % find rightmost point and its segment
                max_x = -1e5;
                max_point = [];
                segment = -1;
                
                for i = 1:size(self.sea_edge,2)
                    s = self.sea_edge(i).points;
                    
                    for j = 1:size(s,2)
                        p = s(j,:);
                        if p(1)>max_x
                            max_x = p(1);
                            max_point = p;
                            segment = i;
                        end
                    end
                end
%                 [max_x, max_point, segment]
%                 plot(max_point(1), max_point(2), 'r*');
                [~, sr] = sort(self.sea_edge(segment).points,1);
                self.sea_edge(segment).points = self.sea_edge(segment).points(sr(:,1),:);
                
                % get penultimate point
                pn = self.sea_edge(segment).points(end-1,:);
                ul = self.sea_edge(segment).points(end,:);
                
                sl = (ul(2)-pn(2))/(ul(1)-pn(1));
                b = ul(2)-(sl*ul(1));
                
                x0 = (ul(1)+pn(1))/2;
                x0 = 1278;
                ex = (sl*x0+b);
%                 plot(x0,ex,'g*');
%                 plot([ul(1) x0],[ul(2) ex],'y--')
                if ~isnan(ex) & ~isinf(ex) & ~all([x0 ex]==ul)
%                     self.sea_edge(segment).points = [self.sea_edge(segment).points; [x0 ex]];
                    self.sea_edge(size(self.sea_edge,2)+1).points=[ul; x0 ex];
                end
                
                
            else
                
                min_x = 1e5;
                min_point = [];
                segment = -1;
                
                for i = 1:size(self.sea_edge,2)
                    s = self.sea_edge(i).points;
                    
                    for j = 1:size(s,2)
                        p = s(j,:);
                        if p(1)<min_x
                            min_x = p(1);
                            min_point = p;
                            segment = i;
                        end
                    end
                end
%                 [min_x, min_point, segment]
                [~, sr] = sort(self.sea_edge(segment).points,1);                
                self.sea_edge(segment).points = self.sea_edge(segment).points(sr(:,1),:); % sort segment
                
                % get penultimate point
                pn = self.sea_edge(segment).points(1,:);
                ul = self.sea_edge(segment).points(2,:);
                
                sl = (ul(2)-pn(2))/(ul(1)-pn(1));
                b = ul(2)-(sl*ul(1));
                
                x0 = 1;
                ex = (-sl*x0+b);
%                 plot(x0,ex,'g*');
%                 plot(pn(1), pn(2), 'r*');
%                 plot([pn(1) x0],[pn(2) ex],'y--')
                if ~isnan(ex) & ~isinf(ex) & ~all([x0 ex]==pn)
%                     self.sea_edge(segment).points = [[x0 ex]; self.sea_edge(segment).points];
                    % add new segment
                    self.sea_edge(size(self.sea_edge,2)+1).points=[x0 ex; pn];
                end
                
            end
%             self.sea_edge(segment).points
            self.display_sea_edge();
            self.save_annotations();
            
        end
        
        function track_annotation_classes(self)
            % should be mapped to a button
            % takes annotations at current frame and uses the Hungraian
            % algorithm to copy annotation classes through sequential
            % frames
            
            pos = self.current_position;
            
            for i = pos:size(self.image_names, 2)-1
                self.current_position = i;
                self.load_annotations();
                o1 = self.obstacles;
                self.current_position = i+1;
                self.load_annotations();
                o2 = self.obstacles;
                
                H = zeros(numel(o1),numel(o2));
                
                if ~isempty(H)
                    for x = 1:numel(o1)
                        for y = 1:numel(o2)
                            p1 = o1(x).polygon(1:2);
                            p2 = o2(y).polygon(1:2);
                            d = norm(p1-p2);
                            H(x,y)=d;
                        end
                    end
                    r = munkres(H);
                    for j = 1:numel(r)
                        if r(j)~=0
                            self.obstacles(r(j)).class = o1(j).class;
                        end

                    end
                    self.save_annotations();
                end

            end
            
            self.current_position = pos-1;
            process_next_frame(self,1);
            
        end
        
        function load_annotations(self)
            self.obstacles = {};
            self.sea_edge = [];
            self.sea_edge_handle = [];
            disp('loading annotations')
            image_file = fullfile(self.dataset_path, 'frames',self.image_names{self.current_position});
            fn = strsplit(image_file,filesep); fn = fn{end}(1:end-4)
            self.annotation_file = fullfile(self.dataset_path, self.annotationsFolderName, [ fn, '.mat' ]);
            
            
            % if .mat doesn't exist, create it, then load data from unrectified .txt file
            if ~exist(self.annotation_file,'file')
                sprintf('Annotation file "%s" not found!', self.annotation_file)
                disp('loading .txt')
                
%                 self.frame
                nm = self.frame;
                fn = fullfile(self.dataset_path, 'annotations_unrectified',sprintf('%08d.txt', nm));
               
                [self.obstacles, self.sea_edge] = read_annotations_from_txt(fn, 0);

                self.save_annotations();
                
            else % else, just load from .mat file
                disp('loading .mat')
                tmp = load(self.annotation_file);
                self.obstacles = tmp.obstacles;
                self.sea_edge = tmp.sea_edge;
                
                if ~isstruct(self.sea_edge)
                    disp('sea edge not struct!')
                    tmp = self.sea_edge;
                    self.sea_edge = {};
                    self.sea_edge(end+1).points = tmp;
                    self.save_annotations();
                end
                
                % add class if non existent
                for i = 1:numel(self.obstacles)
                    if ~isfield(self.obstacles(i), 'class') | isempty(self.obstacles(i).class)
                        self.obstacles(i).class = 'other';
                    end                    
                end
                
                for i = 1:numel(self.obstacles)
                    
                    if ~strcmp(class(self.obstacles(i).frame),'double')
                        
                        self.obstacles(i).frame = str2num(self.obstacles(i).frame);
                    end
                    
                end
                
%                 read_txt_and_plot(txt_path, self.I, listing_corresponding_txts(end).name);
%                 drawnow;
            end
            
        end
        
        function update_display (self)
            % Draw image
            figure(self.figure_main);
            clf();
            imshow(self.I, 'Border', 'tight');
            hold on;
            
            % Update annotations
            self.display_annotations();
%             self.update_current_class_selection();
            self.display_sea_edge();
            set(gcf, 'Position', get(0, 'Screensize')); % force full screen
        end
        
        function display_annotation_info(self)
            for i = 1:size(self.line_handles, 2)
                delete(self.line_handles{i});
            end
            if self.active_type==0 & self.active_idx>0
                idx = self.active_idx;
                class = self.obstacles(idx).class;
                idx = ismember(self.classes, class);
                line_spacing = 50;
                tab_offset = 40;
                x = self.text_offset_x;
                y = self.text_offset_y+line_spacing;
                self.line_handles{1} = text(x, y, 'current annotation:', 'Color', 'k', 'FontSize', self.fontsize);
                self.line_handles{2} = text(x+tab_offset, y+line_spacing, class,  'Color', self.clrs{idx}, 'FontSize', self.fontsize);
                self.line_handles{3} = text(x+tab_offset, y+2*line_spacing, strcat('position: ',mat2str(round(self.obstacles(self.active_idx).polygon))), ...
                    'Color', 'k', 'FontSize', self.fontsize);
            else
                if size(self.line_handles,2)>0
                    for i = 1:size(self.line_handles, 2)
                        delete(self.line_handles{i});
                    end                
                end
            end
        end
        
        function display_annotations (self)
            % Clear old annotations
            delete(self.handle_box(ishandle(self.handle_box)));
%             delete(self.handle_name(ishandle(self.handle_name)));
            
            self.handle_box = nan(1, numel(self.obstacles));
%             self.line_handles = {};
            
            % set primary window
            figure(self.figure_main);
            
            % Display annotations (except during edit)
            if ~self.edit_enabled && numel(self.obstacles)>0
                for i = 1:numel(self.obstacles)
                    entry = self.obstacles(i);
                    line_width = 2;
                    idx = ismember(self.classes, self.obstacles(i).class);
                    color = self.clrs{idx};
%                     if i == self.active_idx
%                         line_width=line_width+2;
%                     end
%                     self.active_idx
                    if i == self.active_idx && self.active_type == 0
                        line_width=line_width+2;
                    end
                    p = entry.polygon;
%                     id = self.obstacles(i).id;
                    if p(3)>0 && p(4)>0
%                         self.handle_box(i) = rectangle('Position',p,'EdgeColor','r','LineWidth',line_width);
                        self.handle_box(i) = rectangle('Position',p,'EdgeColor',color,'LineWidth',line_width);
                    else
                        p(3)=50; p(4)=50;
                        self.obstacles(i).polygon = p;
                        self.handle_box(i) = rectangle('Position',p,'EdgeColor','b','LineWidth',line_width);
                    end
                    
                    
%                     self.handle_name(i)=text(p(1), p(2)-20, id(1:5),'Color', color, 'FontSize', self.fontsize);
                end
            end
            
            % Display selected class
%             for i = 1:size(self.line_handles,2)
%                 if(ishandle(self.line_handles{i}))
%                     delete(self.line_handles{i});
%                 end
%             end
            
%             if self.active_idx > 0 && self.active_type==0
            self.display_annotation_info();
%             end
            
            drawnow();
        end
        
        function update_figure_title (self)
            text = sprintf('#%d/%d: %s', self.current_position, numel(self.image_names), self.image_names{self.current_position});
            set(self.figure_main, 'Name', text);
        end
        
        function delete_annotation (self)
            if self.active_idx == 0
                return;
            end
            
            if self.active_type==0
                % delete obstacle
                self.obstacles = [self.obstacles(1:self.active_idx-1), self.obstacles(self.active_idx+1:end)];
            else
                % delete sea edge
                if numel(self.sea_edge)==1
                    self.sea_edge = struct();
                else
                    self.sea_edge = [self.sea_edge(1:self.active_idx-1), self.sea_edge(self.active_idx+1:end)];
                end
                
            end
            
            %Update
            self.active_idx = 0;
            self.display_annotations();
            self.display_sea_edge();
            
            %% Save annotations
            self.save_annotations();
            
        end
        
        function new_annotation (self)
            self.edit_enabled = true;
            instanceClassName = self.classes{self.selected_class_idx};
            
            %Get polygon
            polygon_handle = imrect(gca);
            polygon = wait(polygon_handle);
            delete(polygon_handle);
            
            if ~isempty(polygon)
                newAnnoInd = numel(self.obstacles) + 1;
                self.obstacles(newAnnoInd).frame = self.frame;
                self.obstacles(newAnnoInd).class = instanceClassName;
                self.obstacles(newAnnoInd).polygon = polygon;
                
                % Update
                
                %% Save annotations
                self.save_annotations();
            end
            self.edit_enabled = false;
            self.display_annotations();
        end
        
        function edit_annotation (self)
            if self.active_idx == 0
                return;
            end
            
            self.edit_enabled = true;
            self.display_annotations(); % Update display
            
            figure(self.figure_main);
            
            old_polygon = self.obstacles(self.active_idx).polygon;
            
            % Modify polygon
            polygon_handle = imrect(gca, old_polygon);
            polygon = wait(polygon_handle);
            delete(polygon_handle);
            
            if isempty(polygon)
                polygon = old_polygon;
            end
            
            self.edit_enabled = false;
            
            self.obstacles(self.active_idx).polygon = polygon;
            self.display_annotations();
            
            %% Save annotations
            self.save_annotations();
        end
        
        function save_annotations(self)
            tmp.obstacles = self.obstacles;
            tmp.sea_edge = self.sea_edge;
            save(self.annotation_file, '-struct', 'tmp');
        end
        
        function display_sea_edge (self)
            
            % Delete old handle(s)
%             self.sea_edge_handle
%             delete(self.sea_edge_handle(ishandle(self.sea_edge_handle)));
%             delete(self.sea_edge_handle);
%             self.sea_edge_handle = nan(1, numel(self.sea_edge));
            idx = ishandle(self.sea_edge_handle);
            delete(self.sea_edge_handle(idx));
            self.sea_edge_handle = [];
            
            if isempty(self.sea_edge)
                return;
            end
            
%             self.sea_edge
            
            
            if ~isempty(fieldnames(self.sea_edge))
                for i = 1:numel(self.sea_edge)
                    sea = self.sea_edge(i).points;
                    if ~isempty(sea)
                        line_width = 2;
                        if i == self.active_idx && self.active_type ~= 0
                            line_width=line_width+2;
                        end
%                         self.sea_edge_handle(i) = plot(sea(:,1), sea(:,2), 'Color','y', 'LineWidth', line_width, 'LineStyle','--');
                        self.sea_edge_handle(i) = plot(sea(:,1), sea(:,2), 'Color','g', 'LineWidth', line_width, 'LineStyle','--');
        %                 self.sea_edge_handle(i) = plot(sea(:,1), sea(:,2), 'Color',rand(1,3), 'LineWidth', line_width, 'LineStyle','--');
                    end                
                end
            end
            
        end
        
        function annotate_sea_edge(self)
            self.edit_enabled = true;
            delete(self.handle_box(ishandle(self.handle_box)));
            
%             self.sea_edge_handle = [];
            
            % delete old sea edge handle
%             idx = ishandle(self.sea_edge_handle)
%             delete(self.sea_edge_handle(idx));
%             self.sea_edge_handle = [];            
            
            poly = impoly(gca, 'Closed', false);
            xy = wait(poly);
            delete(poly);
            
            % extend left and right
%             if ~isempty(xy),
%                 % compute the value at image edges if not defined
%                 if xy(1, 1) > 1,
%                     lc = cross([ xy(1,:), 1 ], [ xy(2,:), 1 ]);
%                     x = 1;
%                     y = -(lc(1)*x + lc(3)) / lc(2);
%                     xy = [ x, y; xy ];
%                 end
%                 
%                 if xy(1, end) < size(self.I, 2),
%                     lc = cross([ xy(end-1,:), 1 ], [ xy(end,:), 1 ]);
%                     x = size(self.I, 2);
%                     y = -(lc(1)*x + lc(3)) / lc(2);
%                     xy = [ xy; x, y ];
%                 end
%             end

            % if currently no sea edge segments exist
            if isempty(fieldnames(self.sea_edge))
                self.sea_edge(end).points = xy;
            else
                self.sea_edge(end+1).points = xy;
            end
            
            % Display sea edge
            self.edit_enabled = false;
            self.display_sea_edge();
            self.display_annotations();
            self.save_annotations();
        end
        
        function delete_sea_edge(self)
            % delete old sea edge
            idx = ishandle(self.sea_edge_handle);
            delete(self.sea_edge_handle(idx));
            self.sea_edge_handle = [];
            self.sea_edge = struct();
            self.save_annotations();
        end
        
        function edit_sea_edge(self)
            
            poly = impoly(gca, self.sea_edge, 'Closed', false);
            xy = wait(poly);
            delete(poly);
            if ~isempty(xy)
                % compute the value at image edges if not defined
                if xy(1, 1) > 1
                    lc = cross([ xy(1,:), 1 ], [ xy(2,:), 1 ]);
                    x = 1;
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ x, y; xy ];
                end
                if xy(1, end) < size(self.I, 2)
                    lc = cross([ xy(end-1,:), 1 ], [ xy(end,:), 1 ]);
                    x = size(self.I, 2);
                    y = -(lc(1)*x + lc(3)) / lc(2);
                    xy = [ xy; x, y ];
                end
                self.sea_edge = xy;
            end
            % Display sea edge
            self.display_sea_edge();
            self.save_annotations();
        end
        
        function copy_annotation (self)
            if self.active_idx == 0
                return;
            end
            
            self.copied_annotation = self.obstacles(self.active_idx);
            self.copied_annotation
        end
        
        function copy_all_to_next_frame(self)
            
            
            
        end
        
        function paste_annotation(self)
            
            if ~isempty(self.copied_annotation)
                if self.copied_annotation.frame==self.frame
                    disp('trying to paste annotation in the same frame');
                else
                    disp('pasted annotation');
                    % edit info and insert into current annotations
                    self.copied_annotation.frame = self.frame;
                    self.obstacles(end+1).polygon = self.copied_annotation.polygon;
                    self.obstacles(end).frame = self.copied_annotation.frame;
                    self.obstacles(end).class = self.copied_annotation.class;
                    self.display_annotations();
                end
            end
            
            %% Save annotations
            self.save_annotations();
        end
        
        function zoom_surrounding(self)
            %zoom surrounding of an annotation
            margin=50;
            if self.active_idx~=0
                bb=self.obstacles(self.active_idx).polygon;
                xl=[bb(1)-10-margin bb(1)+bb(3)+margin];
                yl=[bb(2)-10-margin bb(2)+bb(4)+margin];
                xlim(xl);
                ylim(yl);
            end
        end
        
        function mouse_callback (self, ~)
            if self.edit_enabled
                return;
            end
            
            pos = get(gca, 'CurrentPoint'); pos = pos(1, 1:2);
            active_obst = arrayfun(@(x) inpolygon(pos(1), pos(2), [x.polygon(1) x.polygon(1)+x.polygon(3) x.polygon(1)+x.polygon(3) x.polygon(1) x.polygon(1)],...
                [x.polygon(2) x.polygon(2) x.polygon(2)+x.polygon(4) x.polygon(2)+x.polygon(4) x.polygon(2)]), self.obstacles);
%             active_obst
            if ~isempty(fieldnames(self.sea_edge))
                active_sea = arrayfun(@(x) inpolygon(pos(1), pos(2), x.points(:,1),x.points(:,2)), self.sea_edge);
            else
                active_sea = [];
            end
%             active_sea
%             active = [];
            if any(active_obst)
                self.active_type = 0;
                active = active_obst;
                a=find(active);
                b = a(randi(numel(a)));
                active = zeros(1,numel(active));
                active(b)=1;
            else
                self.active_type = 1;
                active = active_sea;
                if ~isempty(active)
                    active = zeros(1,numel(active));
                    active(randi(numel(active_sea)))=1;
                end
                
%                 a=find(active);
%                 b = a(randi(numel(a)));
%                 active = zeros(1,numel(active));
%                 active(b)=1;
            end
            
            active = find(active);
            
            if isempty(active)
                self.active_idx = 0;
            else
                self.active_idx = active;
            end
            
            self.display_annotations();
            self.display_sea_edge();
%             self.active_type
        end
        
        function keyboard_callback (self, event)
            % Do not interfere with built-in keyboard bindings
            if self.edit_enabled
                return
            end
            
            switch event.Key
                case { 'a', 'leftarrow' }
                    process_next_frame(self, -1);
                case { 's', 'rightarrow' }
                    process_next_frame(self, +1);
                case { 'f' }
                    answer = inputdlg({ 'Enter image number:' }, 'Jump to image', 1, { num2str(self.current_position) });
                    new_frame = str2double(answer{1});
                    
                    if new_frame < 1 || new_frame > numel(self.image_names)
                        msgbox('Invalid image number', 'Error', 'error');
                        return;
                    end
                    
                    self.current_position = new_frame - 1;
                    process_next_frame(self, +1);
                case { 'e' }
                    self.edit_annotation();
                case { 'q' }
                    clc;
                    thr = 16;
                    tmp = {};
                    
                    
                    for i = 1:numel(self.obstacles)
%                         i
                        p = self.obstacles(i).polygon;
                        a = sqrt(p(3)*p(4));
                        a
                        if a>thr
%                             a
%                             self.obstacles = [self.obstacles(1:i-1), self.obstacles(i+1:end)];
%                             tmp(end+1)=self.obstacles(i);
                            tmp(end+1).polygon=self.obstacles(i).polygon;
                            tmp(end).frame=self.obstacles(i).frame;
                            tmp(end).class=self.obstacles(i).class;
                        end
                        
                    end
                    self.obstacles = tmp;
                    self.update_display();
                    self.save_annotations();
                    
                case { 'delete' }
                    self.delete_annotation();
                case { 'c' }
                    self.copy_annotation();
                case { 'v' }
                    self.paste_annotation();
                case { 'h' }
                    self.annotate_sea_edge();
%                 case { 'j' }
%                     self.edit_sea_edge();
                case { 'k' }
                    self.delete_sea_edge();
                case { 't' }
                    self.track_annotation_classes();
                
                case { 'n' }
                    self.new_annotation();
                case { 'z', 'y' }
                    zoom_surrounding(self);
                case { 'u', 'x' }
                    update_display(self);
                case { 'b' }
                    extrapolate_sea(self, -1);
                case { 'm' }
                    extrapolate_sea(self, 1);
                case { '1','2','3' }
                    
                    if ~isempty(event.Modifier) & strcmp(event.Modifier{:},'shift')
                        self.global_class = self.classes{str2double(event.Key)};
                        set_global_class(self);
                    end
                    
                    if self.active_type==0 && self.active_idx>0
                        new_cls = str2double(event.Key);
                        self.obstacles(self.active_idx).class = self.classes{new_cls};
                        
                        if size(self.line_handles,2)>0
                            for i = 1:size(self.line_handles, 2)
                                delete(self.line_handles{i})
                            end
                        end
                        
                        self.display_annotations();
                        self.save_annotations();
                        
                    end
                    
                    self.selected_class_idx = min(str2double(event.Key), size(self.classes,2));
%                 case {'r'}
%                     sprintf('r')
%                     copy_from_previous(self);
                otherwise
                    event.Key
            end
        end
    end
end
