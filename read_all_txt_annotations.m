
% reads all annotations (from Clickworker) and converts them to .mat (for editing)



close all; clear; clc;

% load annotations for single image
pth = '/media/jon/Home/viamaro_data/modd3/';
% output_dir = 'annotations_final'

sequences = dir(pth); sequences=sequences(3:end);

for i = 1:numel(sequences)
    % for i = 178-21
    
    seq = sequences(i);
    seq.name
    
    if ~strcmp(seq.name,'calibration')
%         seq
        
        frames = dir(fullfile(pth,seq.name,'frames','*.jpg'));
        ann_txt = dir(fullfile(pth,seq.name,'annotations_txt','*.txt'));
        ann_final = dir(fullfile(pth,seq.name,'annotations_final','*.txt'));
        ann = dir(fullfile(pth,seq.name,'annotations','*.mat'));
        %         mkdir(fullfile(pth,seq.name,output_dir));
        
        for j = 1:numel(frames)
            %             ann(j)
            %             ann_txt(j)
            frame = frames(j).name; frame=frame(1:end-4);
            fn_mat = fullfile(pth,seq.name,'annotations',strcat(frame,'.mat'));
            fn_final = fullfile(pth,seq.name,'annotations_final',strcat(frame,'.txt'));
%             fn_cw = fullfile(pth,seq.name,'annotations_txt',strcat(frame,'_.txt'))
            if ~exist(fn_mat,'file')
                % try reading from .txt file from final if it exists
                if exist(fn_final, 'file')
                    [obstacles,sea_edge] = read_annotations_from_txt(fn_final,0);
                else
                    % read from clickworker annotations
                    listing_corresponding_txts = dir(fullfile(pth, seq.name, 'annotations_txt',sprintf('%08d_*.txt', str2num(frame))));
            
                    if(numel(listing_corresponding_txts) > 0)
                        tmp_txt_name = listing_corresponding_txts(end).name;
                        fn_cw = fullfile(pth,seq.name,'annotations_txt',tmp_txt_name);
                        [obstacles,sea_edge] = read_annotations_from_txt(fn_cw,1);
                    end
                end                
                
                % save to .mat
                % create directory
                if ~exist(fullfile(pth,seq.name,'annotations'),'dir')
                    mkdir(fullfile(pth,seq.name,'annotations'));
                end
                
                if ~(exist('obstacles','var'))
                    obstacles = struct();
                end
                if ~(exist('sea_edge','var'))
                    sea_edge = struct();
                end
                tmp.obstacles = obstacles;
                tmp.sea_edge = sea_edge;
                save(fn_mat, '-struct', 'tmp');
                obstacles = struct(); % so it is not copied forward
                sea_edge=struct();
            end
            
            %             check if ok
%             im = imread(fullfile(pth,seq.name,'frames',frames(j).name));
%             imshow(im); hold on;
%             
%             line_width = 2;
%             for k = 1:numel(sea_edge)
%                 sea = sea_edge(k).points;
%                 plot(sea(:,1), sea(:,2), 'Color','y', 'LineWidth', line_width, 'LineStyle','--');
%             end
%             
%             for k = 1:numel(obstacles)
%                 entry = obstacles(k);
%                 line_width = 2;
%                 p = entry.polygon;
%                 rectangle('Position',p,'EdgeColor','r','LineWidth',line_width);
%             end            
            
            
%             break
            
            % check if final exists
            
            % load from final, save to .mat
            
            % check if .mat exists
            
            % if not, load from txt
            
            % then save to .mat
            
            % load annotation for a frame
            %             load(fullfile(pth, seq.name,'annotations',ann(j).name))
            %
            %             fn = ann(j).name; fn = fn(1:end-4);
            
            
            
        end
        
%         break
    end
    
end

