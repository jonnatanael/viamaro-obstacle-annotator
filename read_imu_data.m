function [ roll, pitch, yaw ] = read_imu_data( name )

fileID = fopen(name,'r');
d = fscanf(fileID,'%f');
roll = rad2deg(d(1));
pitch = rad2deg(d(2));
yaw = rad2deg(d(3));
fclose(fileID);

end

