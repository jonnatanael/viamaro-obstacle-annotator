
% finds manual annotations and performs interpolation using CLRS (or w/e) tracker;

close all; clear; clc;

path = '/home/jon/Downloads/modd3/kope104-00030160-00031540/';
seq_name = strsplit(path ,'/'); seq_name=seq_name{end-1};
line_width = 2; fontsize = 20;
step = 10;
classes = {'boat', 'buoy', 'other','ship','yacht','swimmer','bird'};
clrs = {[0 0 1], [1 0.64 0], [1 0 0], [0 1 0], [1 1 1], [1 1 0], [1 0 1]};

res = aggregate_annotations(path);

images = dir(strcat(path,'full/*L.jpg'));
N = numel(images);
% figure(1); clf;

for i = 1:step:N
    i
    
    if i == 131
        i
    elseif i < 131 || i > 131
        continue;
    end

    % check if current and next frame have annotations
    if (isfield(res{i}, 'annotations') && ~isempty(res{i}.annotations) && isfield(res{i+step}, 'annotations') && ~isempty(res{i+step}.annotations))
        ann1 = res{i}.annotations;
        ann2 = res{i+step}.annotations;
        
        for j = 1:numel(ann1)
            id = ann1(j).id;
            index = find(strcmp({ann2.id},id)==1);
            if ~isempty(index)
                p1 = ann1(j).polygon;
                p2 = ann2(index).polygon;
                
                % get image list
                lst = {};
                for k = 1:step
                    lst{end+1}=imread(fullfile(path,'full',images(i+(k-1)).name));
%                     imshow(lst{k});
%                     drawnow;
                end
%                 size(lst)
                % perform tracking
                sprintf('tracking forward..')
                poly1 = track(lst,p1);
                
                sprintf('tracking backward..')
                poly2 = track(lst(end:-1:1),p2);
                poly2 = flipud(poly2);
                
%                 poly1
%                 poly2
%                 return
                
                for k = 1:step-1
                    a.polygon = 0.5*poly1(k,:)+0.5*poly2(k,:);
                    a.class = ann1(j).class;
                    a.frame = res{i+k}.frame;
                    a.id = ann1(j).id;
                    if ~isfield(res{i+k}, 'annotations')
                        res{i+k}.annotations = struct('');
                    end                    
                    res{i+k}.annotations = [res{i+k}.annotations a];
                    res{i+k}.manual = false;
                end
                
            end
        end
    end
end

save(strcat(seq_name,'_annotations.mat'),'res');