

close all; clear; clc;

pth = '/media/jon/Home/viamaro_data/modd3/';
sequences = dir(pth); sequences=sequences(3:end);

% load all annotations
% load_all_annotations(pth,sequences);

fn = 'tasks_shuffled_10.csv';
data = readtable(fn,'Delimiter',';','ReadVariableNames',0);

% lst = 'liste_za_anotacije/Job01-items1-200.xlsx';
% lst = 'liste_za_anotacije/Job02-items201-400.xlsx';
% lst = 'liste_za_anotacije/Job03-items401-600.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03a-items401-450.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03b-items451-500.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03c-items501-550.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03d-items551-600.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03e-items601-650.xlsx';
lst = 'liste_za_anotacije/anotacije_fajli/Job03f-items651-700.xlsx';
% lst = 'liste_za_anotacije/anotacije_fajli/Job03g-items701-750.xlsx';

lista = readtable(lst);
conf_lst = lista{:,2};

% nm = 'clickworker_annotations_batch2'
nm = strsplit(lst, '/'); nm = nm{end}; nm = nm(1:end-5)

% return

mkdir(nm);

figure(1); clf;

for i = 1:size(data,1)
    
    token = data{i,1}{1}
    conf = data{i,2}{1};
    
%     if exist(strcat(nm,'/',token),'dir')
%         continue
%     end
    
    ic = strfind(conf_lst,conf);
    idx = find(not(cellfun('isempty',ic)));
    if isempty(idx)
        continue;
    end
    
    mkdir(strcat(nm,'/',token));
    
    images = {};
    
    for j = 1:10
        %         subplot(3,4,j);
        s = data{i,j+2}{1};
        s = strsplit(s,'/');
        
        seq_id = s{2};
        frame_id = s{3};
        
        a=strcat(pth,seq_id,'/annotations_txt/',frame_id,'*.txt');
        listing_corresponding_txts = dir(a);
        listing_corresponding_txts
        
        if size(listing_corresponding_txts,1)~=0
            tmp_txt_name = listing_corresponding_txts(end).name;
            txt_path = strcat(pth,seq_id,'/annotations_txt/',tmp_txt_name);
            
            s=strcat(pth,seq_id,'/frames/',frame_id,'.jpg');
            if s(end-4)=='L'
                s = strcat(s(1:end-5),s(end-3:end));
            end            
            im = imread(s);
            
            img = read_txt_and_plot(txt_path, im, tmp_txt_name);
            
            %         f=getframe(gcf);
            %         new=f.cdata;
            imwrite(img,strcat(nm,'/',token,'/',int2str(j),'.jpg'));
            %         export_fig(strcat('clickworker_annotations/',token,'/',int2str(j),'.jpg'),'-transparent');
            
            %         break
            %         waitforbuttonpress
           
%         else
%             listing_corresponding_txts
        end
        
        
    end
    %     waitforbuttonpress
    %     break
    
    
end