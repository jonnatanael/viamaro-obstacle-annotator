#include "mexopencv.hpp"
#include <opencv2/tracking.hpp>

using namespace std;
using namespace cv;

vector<Rect2d> do_stuff(vector<Mat> images, Rect2d roi) {
	vector<Rect2d> res;

	Mat frame;
	// create a tracker object
	//TrackerCSRT::Params param;
	//cout << param.histogram_bins << endl;
	//Ptr<Tracker> tracker = TrackerCSRT::create(param);
	TrackerKCF::Params param;
	Ptr<Tracker> tracker = TrackerKCF::create(param);

	tracker->init(images.at(0), roi);
	for (int i = 1; i < images.size(); i++){
		tracker->update(images.at(i), roi);
		res.push_back(roi);
	}

	return res;
}

Rect2d toRect2d(MxArray m){
	return cv::Rect_<double>(m.at<double>(0), m.at<double>(1), m.at<double>(2), m.at<double>(3));
}

// input:
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	// Check number of arguments
	nargchk(nlhs <= 1 && nrhs == 2);

	Rect2d roi = toRect2d(MxArray(prhs[1]));

	// read cell
	mwIndex jcell;
	const mxArray *cell = prhs[0];
	mxArray* cellElement;
	const mwSize* dims = mxGetDimensions(prhs[0]);
	vector<Mat> images;

	for (jcell = 0; jcell < dims[1]; jcell++){
		cellElement = mxGetCell(cell, jcell);
		images.push_back(MxArray(cellElement).toMat());
	}

	vector<Rect2d> rois = do_stuff(images, roi);
	int n = rois.size();

	mxArray* res = mxCreateDoubleMatrix(rois.size(), 4, mxREAL);
	double* x0=mxGetPr(res);

	for (int i = 0; i < n; i++){
		//cout << rois.at(i) << endl;
		Rect2d c = rois.at(i);
		x0[i+(n*0)]=c.x;
		x0[i+(n*1)]=c.y;
		x0[i+(n*2)]=c.width;
		x0[i+(n*3)]=c.height;
	}

	plhs[0] = MxArray(res);
}