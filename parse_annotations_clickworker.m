% sequence ID (string), example: 'kope100-00011830-00012500' [this is
%   actually the name of folder where sequence images are stored]. In this
%   folder subdirectory 'annotations_txt' should be located where all of
%   the annotations .txt files are stored
% show_all (boolean): if true, than display annotations of all users for
%   given image, otherwise show just the newest
function parse_annotations_clickworker(sequence_id, show_all)
    % where dataset files are stored
%     dataset_path = fullfile('E:\modd3_annot', sequence_id);
    dataset_path = fullfile('/media/jon/Home/viamaro_data/modd3', sequence_id);
    dataset_path_txt = fullfile(dataset_path, 'annotations_txt');
    
    if(~exist(dataset_path_txt, 'dir'))
       mkdir(dataset_path_txt);
%        system(sprintf('pscp -i private_key_anotacije.ppk borjab@anot.lmi.link:/var/www/html/anot/borjab/dataset/%s/annotations_txt/* %s', sequence_id, dataset_path_txt));   
        system(sprintf('pscp -i /home/jon/.ssh/id_rsa_vicos jonm@anot.lmi.link:/var/www/html/anot/borjab/dataset/%s/annotations_txt/* %s', sequence_id, dataset_path_txt));
    end
    
    sequence_id_split = strsplit(sequence_id, '-');
    sequence_parent = sequence_id_split{1};
    sequence_frame_start = str2double(sequence_id_split{2});
    sequence_frame_end = str2double(sequence_id_split{3});
    
    for sequence_frame_current = sequence_frame_start : 10 : sequence_frame_end
       tmp_img_name = sprintf('%08d.jpg', sequence_frame_current);
       tmp_img = imread(fullfile(dataset_path, 'frames',tmp_img_name));
       
       fprintf('Showing annotation results for image number: %08d\n', sequence_frame_current);
       
       %tmp_img_name = strsplit(listing_dir(i).name, '.');
       %tmp_img_name = tmp_img_name{1};
       listing_corresponding_txts = dir(fullfile(dataset_path_txt, sprintf('%08d_*.txt', sequence_frame_current)));
       if(numel(listing_corresponding_txts) > 0)
           if(show_all)
               % show annotations of all users
               for txt_file_num = 1 : numel(listing_corresponding_txts)
                   tmp_txt_name = listing_corresponding_txts(txt_file_num).name;
                   txt_path = fullfile(dataset_path_txt, tmp_txt_name);
                   
                   read_txt_and_plot(txt_path, tmp_img, listing_corresponding_txts(txt_file_num).name);
               end
               
           else
               % show only the newest (latest) annotation
               tmp_txt_name = listing_corresponding_txts(end).name;
               txt_path = fullfile(dataset_path_txt, tmp_txt_name);
               
               read_txt_and_plot(txt_path, tmp_img, listing_corresponding_txts(end).name);
               
           end
       
       end
       
       w = waitforbuttonpress;
       close all hidden;
        
    end
end



function read_txt_and_plot(txt_file, img_file, img_name)
    % draw image
    figure; clf;
    imagesc(img_file); axis equal; axis tight; title(img_name, 'Interpreter', 'none'); hold on;
    
    % open reader
    f_reader = fopen(txt_file);

    % matrix of obstacles (n x 4 where each row is x, y, width, height)
    obstacles = [];
    points_x = [];
    points_y = [];
    
    % parse through lines in txt file
    tmp_line = fgetl(f_reader);
    while ischar(tmp_line)
        % Check if current line is for water edge 
        if(~isempty(strfind(tmp_line, 'polyline')))

           % use regex to get points points written between [ and ]
           [edgepoint_start_index, edgepoint_end_index] = regexp(tmp_line, '\[(.*?)\]');
           points_x = str2num(tmp_line(edgepoint_start_index(1)+1:edgepoint_end_index(1)-1));
           points_y = str2num(tmp_line(edgepoint_start_index(2)+1:edgepoint_end_index(2)-1));

        % Or obstacle
        elseif(~isempty(strfind(tmp_line, 'rect')))
           
           % use regex to get info of obstacle
           [obs_start_index, obs_end_index] = regexp(tmp_line, '(:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)');
           
           % update obstacle list
           obstacles = [obstacles; [str2num(tmp_line(obs_start_index(1)+1:obs_end_index(1))), ...
                                    str2num(tmp_line(obs_start_index(2)+1:obs_end_index(2))), ...
                                    str2num(tmp_line(obs_start_index(3)+1:obs_end_index(3))), ...
                                    str2num(tmp_line(obs_start_index(4)+1:obs_end_index(4)))]];

        end
        
        % read new line
        tmp_line = fgetl(f_reader);
    end
    
    % close reader
    fclose(f_reader);
    
    % Plot water edge and obstacles
    plot(points_x, points_y, 'k', 'LineWidth', 3); hold on;
    plot(points_x, points_y, 'g', 'LineWidth', 2); hold on;
    for ob = 1 : size(obstacles, 1);
       rectangle('Position', obstacles(ob, :), 'LineWidth', 3, 'EdgeColor', 'k'); hold on;
       rectangle('Position', obstacles(ob, :), 'LineWidth', 2, 'EdgeColor', 'y'); hold on;
    end

end