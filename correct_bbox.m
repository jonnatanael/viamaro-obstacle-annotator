function p = correct_bbox(p, direction, roll, pitch, f, w)

dy1 = 0;
dy2 = 0;

if ~isnan(roll)
    c = p(1)+p(3)/2;
    dy1 = (c-(w/2))*tand(roll); 
end

if ~isnan(pitch)
    dy2 = f*tand(pitch);
end

if direction==0 % from unstabilized to stabilized        
    p(2)=p(2)+dy1+dy2;
else % from stabilized to unstabilized   
    p(2)=p(2)-dy1-dy2;
end

end

