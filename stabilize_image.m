function [im_, dy] = stabilize_image( im, roll, pitch, f)

    im_ = im;
    dy = nan;

    if ~isnan(pitch)
        dy = f*tand(pitch);
        im_ = imtranslate(im,[0 dy]);
    end
    
    if ~isnan(roll)
        im_ = imrotate(im_,-roll,'bicubic','crop');
    end

end

