
close all; clear; clc;

dataset_path = 'snow';

% List all images
contents = dir(fullfile(dataset_path, '*'));

image_names = {};
for c = 1:numel(contents)
    if contents(c).isdir
        continue;
    end

    [ ~, ~, ext ] = fileparts(contents(c).name);
    if ismember(ext, { '.jpg', '.png', '.ppm', '.jpeg', '.bmp', '.JPG' })
        image_names{end+1} = contents(c).name;
    end
end

idx = 11;
I = imread(strcat(dataset_path,'/',image_names{idx}));

load(strcat(dataset_path,'/','annotations/',image_names{idx}(1:end-4),'.mat'));
r = annotations.polygon(1,:)

figure(1);clf; hold on;
% imshow(I);
% rectangle('Position',r,'EdgeColor','r','LineWidth',3);


t = MOSSETracker(rgb2gray(I), r);

N = 20
for i = 1:N
    i
    I = imread(strcat(dataset_path,'/',image_names{idx+i}));
    t.update(rgb2gray(I),0.5);
%     t.pos

    I_ = t.draw_object(I);
    
    imshow(I_);
%     rectangle('Position',[t.pos-t.siz/2 t.siz],'EdgeColor','r','LineWidth',3);
    
    
    drawnow;
end
