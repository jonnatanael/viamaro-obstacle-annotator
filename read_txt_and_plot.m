function read_txt_and_plot(txt_file, img_file, img_name)
    % draw image
    
    imagesc(img_file); axis equal; axis tight; title(img_name, 'Interpreter', 'none'); hold on;
    
    % open reader
    f_reader = fopen(txt_file);

    % matrix of obstacles (n x 4 where each row is x, y, width, height)
    obstacles = [];
    points_x = [];
    points_y = [];
    
    % parse through lines in txt file
    tmp_line = fgetl(f_reader);
    while ischar(tmp_line)
%         tmp_line
        % Check if current line is for water edge 
        if(~isempty(strfind(tmp_line, 'polyline')))

           % use regex to get points points written between [ and ]
           [edgepoint_start_index, edgepoint_end_index] = regexp(tmp_line, '\[(.*?)\]');
           points_x = str2num(tmp_line(edgepoint_start_index(1)+1:edgepoint_end_index(1)-1));
           points_y = str2num(tmp_line(edgepoint_start_index(2)+1:edgepoint_end_index(2)-1));

        % Or obstacle
        elseif(~isempty(strfind(tmp_line, 'rect')))
           
           % use regex to get info of obstacle
           [obs_start_index, obs_end_index] = regexp(tmp_line, '(:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)|:(\-\d+|\d+)');
           
           % update obstacle list
           obstacles = [obstacles; [str2num(tmp_line(obs_start_index(1)+1:obs_end_index(1))), ...
                                    str2num(tmp_line(obs_start_index(2)+1:obs_end_index(2))), ...
                                    str2num(tmp_line(obs_start_index(3)+1:obs_end_index(3))), ...
                                    str2num(tmp_line(obs_start_index(4)+1:obs_end_index(4)))]];

        end
        
        % read new line
        tmp_line = fgetl(f_reader);
    end
    
    % close reader
    fclose(f_reader);
    
    % Plot water edge and obstacles
    plot(points_x, points_y, 'k', 'LineWidth', 3); hold on;
    plot(points_x, points_y, 'g', 'LineWidth', 2); hold on;
    for ob = 1 : size(obstacles, 1);
       rectangle('Position', obstacles(ob, :), 'LineWidth', 3, 'EdgeColor', 'k'); hold on;
       rectangle('Position', obstacles(ob, :), 'LineWidth', 2, 'EdgeColor', 'y'); hold on;
    end

end