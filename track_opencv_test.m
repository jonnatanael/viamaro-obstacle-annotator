

close all; clear; clc;

% set image and bbox

images = dir('snow/*.jpg');

img = {};
for i = 1:size(images,1)
    img{end+1}=imread(strcat('snow/',images(i).name));
end

% roi = [1169, 394, 100, 100];
% a=cv.track(img(1:5), roi);


figure(1);
imshow(img{1}); hold on;
% h = imrect;roi = wait(h);
roi = [1164,3.849999999999999e+02,130,114];
rectangle('Position',roi,'EdgeColor','b','LineWidth',5);
% waitforbuttonpress;

a=cv.track(img, roi);

for i = 2:size(a,1)
    clf;
    imshow(img{i}); hold on;
    rectangle('Position',a(i,:),'EdgeColor','r','LineWidth',5);
    drawnow;
%     waitforbuttonpress;
end

